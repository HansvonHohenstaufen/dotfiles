"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                    BASIC
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin
call plug#begin()
Plug 'dense-analysis/ale'
Plug 'dhruvasagar/vim-table-mode'
Plug 'jamessan/vim-gnupg'
Plug 'preservim/nerdtree'
Plug 'sainnhe/everforest'
Plug 'vim-scripts/ledger.vim'
Plug 'vimwiki/vimwiki'
call plug#end()

" Basic set
set number relativenumber
set autoindent
set nocompatible
set noswapfile
set splitbelow splitright
set encoding=utf8

" Let
let mapleader=" "
imap jj <Esc>
"let g:netrw_dirhistmax=0
"let g:GPGFilePattern='*.\(gpg\|asc\|pgp\)\(.wiki\)\='
"let g:GPGPreferArmor=1
"let g:GPGDefaultRecipients=["mail_to_use_by_gpg@mail.com"]

" File type
filetype indent on
filetype plugin on

filetype on
syntax on

" Viminfo
set viminfo=%,<800,'10,/50,:100,h,f0,n/tmp/.viminfo

" Theme
set background=dark
set termguicolors
let g:everforest_background='hard'
colorscheme everforest

" Completation
set wildmenu
set wildmode=list:longest
set wildignore=*.jpg,*.png,*.gif,*.pdf,*.pyc

" Status line
set statusline=
set statusline+=\ %F\ %M\ %Y\ %R
set statusline+=%=
set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %v\ percent:\ %p%%
set laststatus=2

" Save with doas
command W :execute ':silent w !doas tee % > /dev/null' | :edit!
filetype plugin indent on
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                    CUTOFF
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Split
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

map <leader>l :vsp<CR>
map <leader>j :sp<CR>

" Whitespace
autocmd BufwritePre * %s/\s\+$//e

" See tab and 2 or more spaces
set listchars=tab:\ \ \|,multispace:-
set list

" Clipboard
vnoremap <C-c> "+y
map <C-p> "+p

" Max 79 columns
match ErrorMsg '\%>79v.\+'
autocmd WinEnter * match ErrorMsg '\%>80v.\+'
set textwidth=79
set wrapmargin=1
" Enable or disable limit 79 columns
map <leader>a :match ErrorMsg ''<CR>
	\:set textwidth=0<CR>
map <leader>A :match ErrorMsg '\%>79v.\+'<CR>
	\:set textwidth=79<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                PLUGIN CONFIG
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Spell
map <leader>e :syntax spell toplevel<CR>:setlocal spell! spelllang=es<CR>
map <leader>i :syntax spell toplevel<CR>:setlocal spell! spelllang=en_us<CR>
" Tree
map <leader>f :NERDTree<CR>
map <leader>r :NERDTreeRefreshRoot<CR>
" Exit Vim if NERDTree is the only window remaining.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 &&
	\ exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" Vimwiki
let g:vimwiki_table_auto_fmt=0
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                    FILES
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"	LaTeX
autocmd BufRead,BufNewFile *.tex set filetype=tex
"	Octave
autocmd BufRead,BufNewFile *.m set filetype=octave
"	Spice
autocmd BufRead,BufNewFile *.cir set filetype=spice
autocmd BufRead,BufNewFile *.lib set filetype=spice
