#!/usr/bin/env zsh
###############################################################################
#                                    BASIC
###############################################################################
autoload -U colors && colors
autoload -U compinit
zstyle ':completion:*' menu select
zstyle -e ':completion:*' special-dirs \
	'[[ $PREFIX = (../)#(|.|..) ]] && reply=(..)'
zmodload zsh/complist
compinit
_comp_options+=(globdots)
###############################################################################
#                                     VIM
###############################################################################
bindkey -v
export KEYTIMEOUT=1

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char
#
#	CHANGE CURSOR BY VI MODES
#
function zle-keymap-select
{
	if [[ ${KEYMAP} == vicmd ]] ||
			[[ $1 = 'block' ]]; then
		echo -ne '\e[1 q'
	elif [[ ${KEYMAP} == main ]] ||
			[[ ${KEYMAP} == viins ]] ||
			[[ ${KEYMAP} = '' ]] ||
			[[ $1 = 'beam' ]]; then
		echo -ne '\e[5 q'
	fi
}

zle -N zle-keymap-select
zle-line-init() {
	zle -K viins
	echo -ne "\e[5 q"
}

zle -N zle-line-init
echo -ne '\e[5 q'
preexec() { echo -ne '\e[5 q' ;}
#
# 	EDIT LINE WITH VIM
#
autoload edit-command-line; zle -N edit-command-line
###############################################################################
#                                     GIT
###############################################################################
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
precmd() {
	vcs_info
}
setopt prompt_subst
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
+vi-git-untracked(){
	if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
		git status --porcelain | grep '??' &> /dev/null ; then
		hook_com[staged]+='T'
	fi
}
zstyle ':vcs_info:git*' formats \
"─[%{$fg[green]%}%r/%S%{$fg[grey]%}%{$fg[red]%}]─[\
%{$fg[blue]%}%b%%{$fg[red]%}]─[%{$reset_color%}%m%u%c%{$reset_color%}%B%{$fg[red]%}]"
PROMPT='${vcs_info_msg_0_}'
###############################################################################
#                                PROXY CHAINS
###############################################################################
proxychains() {
	command proxychains "$@"
}
compdef proxychains=command
###############################################################################
#                               EXTERNAL SOURCE
###############################################################################
source "$HOME/.config/aliasrc"
source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh 2>/dev/null
# PROMT
PS1="%B%{$fg[red]%}┌─[%{$fg[green]%}%n@%M%{$fg[red]%}]─[%{$fg[blue]%}%~\
%{$fg[red]%}]${PS1}
%B%{$fg[red]%}└──%{$fg[red]%}[ %{$fg[blue]%}$ %{$reset_color%}%b"
###############################################################################
#                                   EXPORTS
###############################################################################
export GPG_TTY=$(tty)
