" Local
setlocal list&

let g:table_mode_verbose=0
" Markdown != Vimwiki
if expand('%:e') == 'wiki'
	let g:table_mode_header_fillchar='='
endif

map <leader>t :TableModeEnable<CR>
