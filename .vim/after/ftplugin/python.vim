" Tabulate and spaces
setlocal tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab

" Plugin
command! EnableALE let g:ale_enabled = 1 | ALEEnable

let g:ale_lint_delay = 10000
let g:ale_linters = {'python': ['pylint', 'flake8'],}
let g:ale_python_flake8_options = '--append-config ~/.config/flake8rc'

" Compile
map <leader>m :w<CR>:!scompiler --python %<CR>
map <leader>p :w<CR>:!scompiler --python -p<CR>
map <leader>c :w<CR>:!scompiler --clear<CR><CR>
