"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                    BASIC
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
setlocal tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab
command! EnableALE let g:ale_enabled = 1 | ALEEnable
" Compile
map <leader>m :w<CR>:!scompiler --latex<CR>
map <leader>s :w<CR>:!scompiler --latex -s<CR>
map <leader>b :w<CR>:!scompiler --latex -b<CR>
map <leader>t :w<CR>:!chktex %<CR>
map <leader>c :w<CR>:!scompiler --clear %<CR><CR>

filetype indent off
set smartindent
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                    MACRO
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap ,al
	\ \begin{align*}<Enter>
	\\end{align*}
	\<Enter><esc>kO
inoremap ,de
	\ \begin{description}<Enter>
	\\item<Space>[]<Enter>
	\\end{description}
	\<esc>k$i
inoremap ,en
	\ \begin{enumerate}<Enter>
	\\item<Space><Enter>
	\\end{enumerate}
	\<esc>k$a
inoremap ,eq
	\ \begin{equation}<Enter>
	\\end{equation}
	\<Enter><esc>kO
inoremap ,fb
	\ \begin{subfigure}[h]{0.45\textwidth}<Enter>
	\<tab>\centering<Enter>
	\\includegraphics[<Enter>
	\<Tab>width=\linewidth,<Enter>
	\height=\linewidth,<Enter>
	\keepaspectratio]<Enter>
	\<Tab>{.png}<Enter>
	\\caption{}<Enter>
	\\end{subfigure}<esc>Bhhhxxxkxxkxllli
inoremap ,fi
	\ \begin{figure}[H]<Enter>
	\\centering<Enter>
	\\includegraphics[<Enter>
	\<Tab>width=0.6\linewidth,<Enter>
	\height=0.6\linewidth,<Enter>
	\keepaspectratio]<Enter>
	\<Tab>{.png}<Enter>
	\\caption{}<Enter>
	\\end{figure}<esc>Bhhxxkxxkxlli
inoremap ,fr
	\ \begin{frame}{}<Enter><Enter>
	\\end{frame}<esc>kk$i
inoremap ,it
	\ \begin{itemize}<Enter>
	\\item<Space><Enter>
	\\end{itemize}
	\<esc>k$a
inoremap ,la
	\ \label{}<esc>i
inoremap ,ls
	\ \lstinputlisting[<Enter>
	\<Tab>language<tab>= ,<Enter>
	\caption<tab><tab>= {}<Enter>
	\]{.}<esc>2hi
inoremap ,mfi
	\ \mfigure{.png}{}<esc>6hi
inoremap ,mfl
	\ \mfigurel{.png}{}{}<esc>8hi
inoremap ,mfs
	\ \mfigures{.png}{}{}<esc>8hi
inoremap ,ta
	\ \begin{table}[H]<Enter>
	\\centering<Enter>
	\\begin{tabular}{}<Enter>
	\<tab>\toprule<Enter>
	\\midrule<Enter>
	\\bottomrule<Enter>
	\<esc>I
	\\end{tabular}<Enter>
	\\caption{}<Enter>
	\\end{table}<Enter>
inoremap ,tb
	\ \textbf{}<esc>i
inoremap ,ti
	\ \textit{}<esc>i
inoremap ,tr
	\ \textrm{}<esc>i
inoremap ,re
	\ Ecuación~\ref{eq:}<esc>i
inoremap ,rf
	\ Figura~\ref{fig:}<esc>i
inoremap ,rl
	\ Lista~\ref{lst:}<esc>i
inoremap ,rt
	\ Tabla~\ref{tab:}<esc>i
inoremap ,si
	\ \qty{}{}<esc>2hi
inoremap ,su
	\ \unit{}<esc>i
inoremap ,sp
	\ \begin{equation}<Enter>
	\\begin{split}<Enter>
	\\end{split}<Enter>
	\\end{equation}
	\<esc>kO
