# Dotfiles

These are my dotfiles and config.

Setting and configuration for:

- git
- LaTeX (macros)
- vim
- zsh

# Git

The configuration is divided in two sections.

## Global

Basic config.

| Key            | Config  | Description                            |
|----------------|---------|----------------------------------------|
| GPG            | gpg2    | Sign commits                           |
| Default Branch | master  | Initial branch                         |
| Editor         | vim     |                                        |
| Pager          | less    | It's better when the graph is too long |
| Merge tool     | vimdiff | Resolve conflicts with more details    |

Alias.

| Alias | Description                    |
|-------|--------------------------------|
| tree  | Show how a branches tree       |
| ls    | Show all files from repository |
| lt    | Show files in stage            |
| sw    | Switch branch                  |

## Local

In each repository you add the next data in `.git/config`

- Email.
- User.
- Sing key.

You have to add a Hosts config in your `.ssh/config` for each ssh key.

# LaTeX

Some macros to figures with/without label.

| Macro                                           | Description                  |
|-------------------------------------------------|------------------------------|
| `\mfigure[1.0]{FILE}{CAPTION}`                  | figure                       |
| `\mfigurel[1.0]{FILE}{CAPTION}{LABEL}`          | figure with label            |
| `\mfigures[1.0]{FILE}{CAPTION}{SOURCE}`         | figure with source           |
| `\mfiguresl[1.0]{FILE}{CAPTION}{SOURCE}{LABEL}` | figure with source and label |

When [1.0] is the line width.

# Vim

## Cutoff

Global in *normal* mode.

| Key    | Description                     |
|--------|---------------------------------|
| jj     | From insert mode to normal mode |
|--------|---------------------------------|
| Ctrl-h | Move to left window             |
| Ctrl-j | Move to bottom window           |
| Ctrl-k | Move to top window              |
| Ctrl-l | Move to right window            |
|--------|---------------------------------|
| W      | Save usage `doas`               |

Global in *visual* mode.

| Key    | Description          |
|--------|----------------------|
| Ctrl-c | Copy to Clipboard    |
| Ctrl-p | Paste from Clipboard |

Global with `leader` key *space*.

| Key | Description                  |
|-----|------------------------------|
| l   | Vertical split               |
| j   | Horizontal split             |
|-----|------------------------------|
| a   | Remove limit from columns    |
| A   | Restore limit from columns   |
|-----|------------------------------|
| e   | Enable/Disable check spanish |
| i   | Enable/Disable check english |
|-----|------------------------------|
| f   | Go to file explore           |

## Plugins

Use `vim-plug` to manager plugins.

| Plugin                       | Description                                         |
|------------------------------|-----------------------------------------------------|
| `dense-analysis/ale`         | Check pep8 in python files, tex, sh, other          |
| `dhruvasagar/vim-table-mode` | Simple and beautiful tables in markdown and vimwiki |
| `jamessan/vim-gnupg`         | Open/encrypt wiki files with gpg                    |
| `preservim/nerdtree`         | Tree files bar explore                              |
| `sainnhe/everforest`         | Comfortable and pleasant color theme                |
| `vim-scripts/ledger`         | Syntax for ledger files                             |
| `vimwiki/vimwiki`            | Syntax and beautiful vimwiki                        |

## Linters

| Language | Linter                |
|----------|-----------------------|
| `python` | `pylint` and `flake8` |
| `LaTeX`  | `chktex`              |
| `bash`   | `shellcheck`          |

## Install

Copy the `.vimrc` and `.vim` to user home directory.

The first time run `vim`, execute the next command and exit.

```
PlugInstall
```

# Zsh

The basic configurations.

- Vim mode.
- History in `/tmp`.
- Git extension.
- Alias files
- LS personal colors.
- Auto start dwm in tty1.
- NNN basic config.
- Add `~/.local/bin` to `PATH`.
